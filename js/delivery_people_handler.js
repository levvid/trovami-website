var APP_ID = "AD95965E-DDD3-F314-FFD3-D114AEEA7800";
var SECRET_KEY = "E8E727BB-2AB3-335A-FFAA-213F146D9C00";
var VERSION = "v1";
init();

function init() {

  Backendless.initApp(APP_ID, SECRET_KEY, VERSION);
  console.log("Loaded deliveryPeople handler");
  fetchingFirstPageAsync();
}


function DeliveryPeople(args) {
  args = args || {};
  this.Name = args.Name || "";
  this.Company = args.Company || "";
  this.PhoneNumber = args.PhoneNumber || "";
}



function handleResponse(deliveryPeople){
    // console.log("Loaded " + deliveryPeople.data.length + "restaurant objects");
    // console.log("Total restaurants in the Backendless storage - " + deliveryPeople.totalObjects);
    var deliveryPeopleTable = '<tr><th>Name</th><th>Company</th><th>Phone Number</th></tr><tbody>';
    
    for(var i = 0; i < deliveryPeople.data.length; i++) {
      var companyNameNew = deliveryPeople.data[i].Company;
      console.log("deliveryPeople name = " + deliveryPeople.data[i].Name);
      deliveryPeopleTable+= '<tr>';
      deliveryPeopleTable+= '<td>' + deliveryPeople.data[i].Name + '</td>';
      deliveryPeopleTable+= '<td>' + deliveryPeople.data[i].Company + '</td>';
      deliveryPeopleTable+= '<td>' + deliveryPeople.data[i].PhoneNumber + '</td>';
      deliveryPeopleTable+= '</tr>';
    }

    deliveryPeopleTable +='<tr><td><input type="text" name="name" id="name" placeholder="Name">';
    deliveryPeopleTable += '</td><td>' + companyNameNew;
    deliveryPeopleTable += '</td><td><input type="text" name="phonenumber" id="phonenumber" placeholder="Phonenumber"></td>';
    deliveryPeopleTable+='</tbody>';
    document.getElementById('deliveryPeopleTable').innerHTML = deliveryPeopleTable;

    // console.log("Total time (ms) - " + ((new Date()).getMilliseconds() - startTime ));
    console.log("Success");
  }

  function handleFault(backendlessFault){
    console.log( "Server reported an error - ");
    console.log(backendlessFault.message);
    console.log(backendlessFault.statusCode);
  }

  function fetchingFirstPageAsync(){
    try{

        //Backendless.Persistence.of( DeliveryPeople ).find(callback);
        var user = Backendless.UserService.getCurrentUser();
        console.log("User details are: " + user==null);
        if( user != null ){
          var companyName = user[ "name" ];
          console.log( "Company Name: "+ companyName );

          retrieveDeliveryPersons(companyName);
        }
        else{
          console.log( "User hasn't been logged" );
        } 

        
      }
      catch(e){
        throw e.message;
      }
      finally{
        console.log("============ Fetching first page using the ASYNC API ============");
      }
    }


    function retrieveDeliveryPersons(company){
      var deliveryPeopleStorage = Backendless.Persistence.of( DeliveryPeople );
      var query = new Backendless.DataQuery();
      query.condition = "Company = '" + company + "'";
      var deliveryPeople = deliveryPeopleStorage.find( query );
      handleResponse(deliveryPeople);

      // try{
      //   Backendless.Persistence.of(DeliveryPeople).find(callback)
      // }
      // catch(e){
      //   console.log("Error retrieving delivery people" + e.message);
      // }
    }
    function addDeliveryPerson(){
      var name = document.getElementById("name").value;
      var phonenumber = document.getElementById("phonenumber").value;
      console.log("This is the details: " + name + "  " + phonenumber);
      var currentUser = Backendless.UserService.getCurrentUser();
      var companyName = currentUser[ "name" ];
      var deliveryPerson = new DeliveryPeople({
        Name: name,
        Company: companyName,
        PhoneNumber: phonenumber,
      });
      var savedDeliveryPerson = Backendless.Persistence.of( DeliveryPeople ).save( deliveryPerson );
      console.log("Saved deliveryPerson");
      fetchingFirstPageAsync();
      console.log("refreshing table...");
    }





var APP_ID = "AD95965E-DDD3-F314-FFD3-D114AEEA7800";
var SECRET_KEY = "E8E727BB-2AB3-335A-FFAA-213F146D9C00";
var VERSION = "v1";
init();

function init() {

  Backendless.initApp(APP_ID, SECRET_KEY, VERSION);
  console.log("Loaded deliveries handler");
  fetchingFirstPageAsync();
}


function Deliveries(args) {
  args = args || {};
  this.deliverStatus = args.deliverStatus || "";
  this.DeliveryPerson = args.DeliveryPerson || "";
  this.DeliveryRecipient = args.DeliveryRecipient || "";
  this.DeliveryPersonName = args.DeliveryPersonName || "";
}


function DeliveryPeople(args) {
  args = args || {};
  this.Name = args.Name || "";
  this.Company = args.Company || "";
  this.PhoneNumber = args.PhoneNumber || "";
}



function handleResponse(deliveries){
    // console.log("Loaded " + deliveryPeople.data.length + "restaurant objects");
    // console.log("Total restaurants in the Backendless storage - " + deliveryPeople.totalObjects);
    var deliveryTable = '<tr><th>Delivery Person Name</th><th>Delivery Person Number</th><th>Recipient Phone Number</th><th>Delivery Status</th></tr><tbody>';
    
    for(var i = 0; i < deliveries.data.length; i++) {
      var deliveryPerson = deliveries.data[i].DeliveryPerson;
      console.log("deliveries # = " + deliveries.data[i].DeliveryPerson);
      deliveryTable+= '<tr>';
      deliveryTable+= '<td>' + deliveries.data[i].DeliveryPersonName+ '</td>';
      deliveryTable+= '<td>' + deliveries.data[i].DeliveryPerson + '</td>';
      deliveryTable+= '<td>' + deliveries.data[i].DeliveryRecipient + '</td>';
      var statusCode = deliveries.data[i].deliverStatus;
      var statusUserEnd = "Pending";
      if (statusCode == 1) {
        statusUserEnd = "Delivered";
      };
      deliveryTable+= '<td>' + statusUserEnd + '</td>';
      deliveryTable+= '</tr>';
    }

    deliveryTable +='<tr><td><input type="text" name="name" id="deliveryPersonName" placeholder="Name">';
    deliveryTable += '</td><td id = "deliveryPersonNumber">';
    deliveryTable += '</td><td><input type="text" name="phonenumber" id="recipientPhoneNumber" placeholder="Phonenumber"></td>';
    deliveryTable += '</td><td>Pending</td></tr>';
    deliveryTable+='</tbody>';
    document.getElementById('deliveriesTable').innerHTML = deliveryTable;

    // console.log("Total time (ms) - " + ((new Date()).getMilliseconds() - startTime ));
    console.log("Success");
  }

  function handleFault(backendlessFault){
    console.log( "Server reported an error - ");
    console.log(backendlessFault.message);
    console.log(backendlessFault.statusCode);
  }

  function fetchingFirstPageAsync(){
    try{

        //Backendless.Persistence.of( DeliveryPeople ).find(callback);
        var user = Backendless.UserService.getCurrentUser();
        console.log("User details are: " + user==null);
        if( user != null ){
          var companyName = user[ "name" ];
          console.log( "Company Name: "+ companyName );

          
        }
        else{
          console.log( "User hasn't been logged" );
        } 

        
      }
      catch(e){
        throw e.message;
      }
      finally{
        console.log("============ Fetching first page using the ASYNC API ============");
        retrieveDeliveries(user);
      }
    }

    /**Add: 
    * Ability to navigate to next page etc
    * */
    function retrieveDeliveries(currentUser){
      var deliveriesStorage = Backendless.Persistence.of( Deliveries );
      var query = new Backendless.DataQuery();
      var PAGESIZE = 50;
      query.options = {
        pageSize:PAGESIZE
      };
      query.condition = "ownerId = '" + currentUser["ownerId"] + "'";
      console.log("ownerId = " + currentUser["ownerId"] );
      var deliveries = deliveriesStorage.find( query );
      console.log("retrieveDeliveries done...calling handleResponse");
      handleResponse(deliveries);
    }


    function addDelivery(){
      var deliveryPersonName = document.getElementById("deliveryPersonName").value;
      var deliveryPersonNumber = "6073792033";
      var recipientPhoneNumber = document.getElementById("recipientPhoneNumber").value;
      console.log("This is the details: " + deliveryPersonName + "  " + recipientPhoneNumber);
      var currentUser = Backendless.UserService.getCurrentUser();
      var companyName = currentUser[ "name" ];
      if (deliveryPersonName == "") {
        alert("Please enter a valid Delivery Person Name");
        
      }else if(recipientPhoneNumber == ""){

        alert("Please enter a valid Recipient Phone Number");
      }else{
        var delivery = new Deliveries({
          deliverStatus: "0",
          DeliveryPerson: deliveryPersonNumber,
          DeliveryRecipient: recipientPhoneNumber,
          DeliveryPersonName: deliveryPersonName,
        });
        var savedDelivery = Backendless.Persistence.of( Deliveries ).save( delivery );
        console.log("Saved new delivery");
        fetchingFirstPageAsync();
        console.log("refreshing table...");

      };
      
    }




